/*
 * Copyright (C) 2018 Lucas Ramage <ramage.lucas@protonmail.com>
 *
 * This work is licensed under the terms of the GNU GPLv2 or later.
 * See the COPYING file in the top-level directory.
 */

#include <string.h>
#include <stdlib.h>

#include <libco.h>
#include "libretro.h"

#define RETRO_DEVICE_MAPPER RETRO_DEVICE_SUBCLASS(RETRO_DEVICE_KEYBOARD, 2)

#ifndef PATH_MAX_LENGTH
#define PATH_MAX_LENGTH 4096
#endif

#define CORE_VERSION "2.11.50"

#define RETROLOG(msg) printf("%s\n", msg)

int qemu_main(int argc, char **argv, char **envp);

const char *retro_save_directory;
const char *retro_system_directory;
const char *retro_content_directory;

static retro_video_refresh_t video_cb;
static retro_audio_sample_batch_t audio_batch_cb;
static retro_input_poll_t poll_cb;
static retro_input_state_t input_cb;
static retro_environment_t environ_cb;
static retro_log_printf_t log_cb;

void retro_set_video_refresh(retro_video_refresh_t cb) { video_cb = cb; }
void retro_set_audio_sample(retro_audio_sample_t cb) { }
void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb) { audio_batch_cb = cb; }
void retro_set_input_poll(retro_input_poll_t cb) { poll_cb = cb; }
void retro_set_input_state(retro_input_state_t cb) { input_cb = cb; }

static struct retro_variable vars[] = {
    { "qemu_boot", "Boot device; c|d|n"},
    { "qemu_bios", "Bios; "},
    { "qemu_vga", "Graphics Card; cirrus|std"},
    { NULL, NULL },
};

void retro_set_environment(retro_environment_t cb)
{
    environ_cb = cb;

    bool allow_no_game = true;
    environ_cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &allow_no_game);

    cb(RETRO_ENVIRONMENT_SET_VARIABLES, (void*)vars);

    static const struct retro_controller_description pads[] =
    {
       { "Keyboard",   RETRO_DEVICE_MAPPER },
       { NULL, 0 },
    };

    static const struct retro_controller_info ports[] = {
       { pads, 3 },
       { NULL, 0 },
    };
    environ_cb(RETRO_ENVIRONMENT_SET_CONTROLLER_INFO, (void*)ports);

    const char *system_dir = NULL;
    if (environ_cb(RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY, &system_dir) && system_dir)
       retro_system_directory=system_dir;
    if (log_cb)
       log_cb(RETRO_LOG_INFO, "SYSTEM_DIRECTORY: %s\n", retro_system_directory);

    const char *save_dir = NULL;
    if (environ_cb(RETRO_ENVIRONMENT_GET_SAVE_DIRECTORY, &save_dir) && save_dir)
       retro_save_directory = save_dir;
    else
       retro_save_directory=retro_system_directory;
    if (log_cb)
       log_cb(RETRO_LOG_INFO, "SAVE_DIRECTORY: %s\n", retro_save_directory);

    const char *content_dir = NULL;
    if (environ_cb(RETRO_ENVIRONMENT_GET_CONTENT_DIRECTORY, &content_dir) && content_dir)
       retro_content_directory=content_dir;
    if (log_cb)
       log_cb(RETRO_LOG_INFO, "CONTENT_DIRECTORY: %s\n", retro_content_directory);
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{
    (void)port;
    (void)device;
}

static void check_variables()
{
   RETROLOG("Not yet implemented.");
}

#ifndef PATH_SEPARATOR
# if defined(WINDOWS_PATH_STYLE) || defined(_WIN32)
#  define PATH_SEPARATOR '\\'
# else
#  define PATH_SEPARATOR '/'
# endif
#endif

static char* normalizePath(char* aPath)
{
   char *tok;
   static const char replaced = (PATH_SEPARATOR == '\\') ? '/' : '\\';

   for (tok = strchr(aPath, replaced); tok; tok = strchr(aPath, replaced))
      *tok = PATH_SEPARATOR;

   return aPath;
}

//

cothread_t mainThread;
cothread_t emuThread;

bool startup_state_capslock;
bool startup_state_numlock;

void restart_program()
{
   // Not supported; this is used by the CONFIG -r command.
   RETROLOG("Program restart not supported.");
}

// Events
void MIXER_CallBack(void * userdata, uint8_t *stream, int len);

static char *loadPath;
static char *configPath;
static uint8_t audioData[829 * 4]; // 49716hz max
static uint32_t samplesPerFrame = 735;
static bool QEMUwantsExit;
static bool FRONTENDwantsExit;

unsigned currentWidth, currentHeight;

static void retro_start_emulator(void)
{

   check_variables();
   /* Init the configuration system and add default values */

   qemu_main(1, (char *[]){"x"}, (char *[]){"y"});

   /* Load config */

   /* Init all the sections */

   check_variables();

   /* Init done, go back to the main thread */
   co_switch(mainThread);

   if(true)
   {
      RETROLOG("Not yet implemented.");
   }
   else
   {
      RETROLOG("Frontend said to quit.");
      return;
   }

   RETROLOG("QEMU said to quit.");
   QEMUwantsExit = true;
}

static void retro_wrap_emulator(void)
{
   retro_start_emulator();

   co_switch(mainThread);

   while(true)
   {
      RETROLOG("Running a dead emulator.");
      co_switch(mainThread);
   }
}

//

unsigned retro_api_version(void)
{
    return RETRO_API_VERSION;
}

void retro_get_system_info(struct retro_system_info *info)
{
    info->library_name = "QEMU";
#ifdef GIT_VERSION
    info->library_version = CORE_VERSION GIT_VERSION;
#else
    info->library_version = CORE_VERSION;
#endif
    info->valid_extensions = "img|iso";
    info->need_fullpath = false;
    info->block_extract = false;
}

void retro_get_system_av_info(struct retro_system_av_info *info)
{
    info->geometry.base_width = 800; 
    info->geometry.base_height = 600; 
    info->geometry.max_width = 1280;
    info->geometry.max_height = 1024;
    info->geometry.aspect_ratio = (float)4/3;
    info->timing.fps = 60.0;
}

void retro_init(void)
{
   // initialize logger interface
   struct retro_log_callback log;
   if (environ_cb(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &log))
      log_cb = log.log;
   else
      log_cb = NULL;

   if (log_cb)
      log_cb(RETRO_LOG_INFO, "Logger interface initialized... \n");

//   environ_cb(RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, );
   if(!emuThread && !mainThread)
   {
      mainThread = co_active();
      emuThread = co_create(65536*sizeof(void*)*16, retro_wrap_emulator);
   }
   else
   {
      RETROLOG("retro_init called more than once.");
   }
}

void retro_deinit(void)
{
   FRONTENDwantsExit = !QEMUwantsExit;

   if(emuThread)
   {
      // If the frontend says to exit we need to let the emulator run to finish its job.
      if(FRONTENDwantsExit)
      {
         co_switch(emuThread);
      }

      co_delete(emuThread);
      emuThread = 0;
   }
   else
   {
      RETROLOG("retro_deinit called when there is no emulator thread.");
   }
}

bool retro_load_game(const struct retro_game_info *game)
{
    char slash;
    #ifdef _WIN32
    slash = '\\';
    #else
    slash = '/';
    #endif

   if(emuThread)
   {
      if(game)
      {
         // Copy the game path
         loadPath = normalizePath(game->path);

         // Find any config file to load
         if(true)
         {
	    const char *extension;
            if(extension == "conf")
            {
               configPath = loadPath;
            }
            else if(configPath == NULL)
            {
               char *tmpPath = retro_system_directory;
	       
	       strcat(tmpPath, slash);
	       strcat(tmpPath, "qemu");
	       strcat(tmpPath, slash);
	       strcat(tmpPath, "qemu.conf");

	       configPath = normalizePath(tmpPath);

               log_cb(RETRO_LOG_INFO, "Loading default configuration %s\n", configPath);
            }
         }
      }

      co_switch(emuThread);
 //     samplesPerFrame = MIXER_RETRO_GetFrequency() / 60;
      return true;
   }
   else
   {
      RETROLOG("retro_load_game called when there is no emulator thread.");
      return false;
   }

}

bool retro_load_game_special(unsigned game_type, const struct retro_game_info *info, size_t num_info)
{
   if(num_info == 2)
   {
      configPath = normalizePath(info[1].path);
      return retro_load_game(&info[0]);
   }
   return false;
}

void retro_run(void)
{
//   if ( != currentWidth || != currentHeight)
   {
//      log_cb(RETRO_LOG_INFO,"Resolution changed %dx%d => %dx%d\n",
//         currentWidth, currentHeight, , );
      struct retro_system_av_info new_av_info;
      retro_get_system_av_info(&new_av_info);
//      new_av_info.geometry.base_width = ;
//      new_av_info.geometry.base_height = ;
      environ_cb(RETRO_ENVIRONMENT_SET_GEOMETRY, &new_av_info);
//      currentWidth = ;
//      currentHeight = ;
   }

   bool updated = false;
   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
   {
      check_variables();
   }

   if(emuThread)
   {

      // Run emu
      co_switch(emuThread);

      audio_batch_cb((int16_t*)audioData, samplesPerFrame);
   }
   else
   {
      RETROLOG("retro_run called when there is no emulator thread.");
   }
}

// Stubs
void *retro_get_memory_data(unsigned type) { return 0; }
size_t retro_get_memory_size(unsigned type) { return 0; }
void retro_reset (void) { }
size_t retro_serialize_size (void) { return 0; }
bool retro_serialize(void *data, size_t size) { return false; }
bool retro_unserialize(const void * data, size_t size) { return false; }
void retro_cheat_reset(void) { }
void retro_cheat_set(unsigned unused, bool unused1, const char* unused2) { }
void retro_unload_game (void) { }
unsigned retro_get_region (void) { return RETRO_REGION_NTSC; }
