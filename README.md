# qemu-libretro

[Libretro][lr-url] wrapper for [QEMU][q-url]

## Compile

    mkdir build && cd build
    ../configure.libretro
    make

[lr-url]: https://www.libretro.com
[q-url]: https://www.qemu.org