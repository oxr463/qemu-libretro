FROM alpine:latest

RUN apk update && apk add \
      autoconf \
      g++ \
      gcc \
      gdb \
      git \
      glib-dev \
      libdrm-dev \
      libepoxy-dev \
      make \
      mesa-dev \
      musl-dev \
      pixman-dev \
      python \
      strace \
      zlib-dev

WORKDIR /usr/src
RUN git clone https://github.com/libretro/retroarch.git && \
    cd retroarch && ./configure && make && make install

WORKDIR /usr/src/qemu-libretro
COPY . /usr/src/qemu-libretro
CMD /bin/sh -c "mkdir build && cd build && ../configure.libretro --python=/usr/bin/python --enable-debug-info && make"
